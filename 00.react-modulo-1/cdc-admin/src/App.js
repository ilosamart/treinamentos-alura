import React from 'react';
import './css/pure-min.css';
import './css/side-menu.css';
import PubSub from 'pubsub-js';
import { Link } from 'react-router-dom';


class App extends React.Component {

  // constructor() {
  //   super();
  //   this.state = { name: "" };
  // }

  state = {numeroAutores: 0};

  componentDidMount(){  
    PubSub.subscribe('atualiza-lista-autores', (topico, lista) => {
        this.setState({numeroAutores: lista.length});
    });
  };

  render() {

    return (
      <div id="layout">
          <a href="#menu" id="menuLink" className="menu-link">
              <span></span>
          </a>

          <div id="menu">
              <div className="pure-menu">
                  <a className="pure-menu-heading" href="#">MPRS - ReactJS</a>

                  <ul className="pure-menu-list">
                      <li className="pure-menu-item"><Link to="/" className="pure-menu-link">Home</Link></li>
                      <li className="pure-menu-item"><Link to="/autor" className="pure-menu-link">Autores</Link></li>
                      <li className="pure-menu-item"><Link to="/livro" className="pure-menu-link">Livros</Link></li>
                  </ul>
              </div>
          </div>

          <div id="main">
            {this.props.children}
          </div>
      </div>
    );
  }
}

export default App;
