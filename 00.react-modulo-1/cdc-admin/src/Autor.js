import React, { Component } from 'react';
import InputCustomizado from './components/InputCustomizado';
import SubmitCustomizado from './components/SubmitCustomizado';
import $ from 'jquery';
import PubSub from 'pubsub-js';

import TratadorErros from './TratadorErros';

const api = 'http://cdc-react.herokuapp.com/api';

class FormularioAutor extends Component{

    state = {
        nome: '',
        email: '',
        senha: '',
    };

    setNome(evento){
        this.setState({nome:evento.target.value});
    };
    
    setEmail(evento){
        this.setState({email:evento.target.value});
    };
    
    setSenha(evento){
        this.setState({senha:evento.target.value});
    };

    enviaForm(evento) {
        evento.preventDefault();
        console.log(this);
        console.log("dados sendo enviados");
        $.ajax({
          url: api + '/autores',
          contentType: 'application/json',
          dataType:'json',
          type:'post',
          data: JSON.stringify({
            nome: this.state.nome,
            email: this.state.email,
            senha: this.state.senha,
          }),
          success: (novaListagem) => {
            console.log("enviado com sucesso");
            //disparar um aviso geral de novaListagem disponivel
            PubSub.publish('atualiza-lista-autores', novaListagem);
          },
          error: (resposta) => {
              if(resposta.status === 400) {
                  new  TratadorErros().publicarErros(resposta.responseJSON);
              }
          },
          beforeSend: () => {
              PubSub.publish('limpra-erros-form', '');
          }
        });
      };

    render() {
        return (
            <div className="pure-form pure-form-aligned">
                {
                /*
                Referência para sintaxe do 'onSubmit' abaixo,
                necessário ser assim para fazer o 'bind' do 'this' no método.
                https://reactjs.org/docs/handling-events.html
                */
                }
                <form className="pure-form pure-form-aligned" onSubmit={(e) => this.enviaForm(e)} method="post">
                <InputCustomizado id="nome" type="text" name="nome" value={this.state.nome} onChange={(e) => this.setNome(e)} label="Nome"/>                                              
                <InputCustomizado id="email" type="email" name="email" value={this.state.email} onChange={(e) => this.setEmail(e)} label="Email"/>                                              
                <InputCustomizado id="senha" type="password" name="senha" value={this.state.senha} onChange={(e) => this.setSenha(e)} label="Senha"/>
                <SubmitCustomizado caption="Gravar"/>
                </form>             

            </div>
        )
    }
}


class TabelaAutores extends Component {

    render() {
        return (
            <div>            
                <table className="pure-table">
                <thead>
                    <tr>
                    <th>Nome</th>
                    <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                    {
                    this.props.lista.map((autor) => {
                        return (
                        <tr key={autor.id}>
                            <td>{autor.nome}</td>
                            <td>{autor.email}</td>
                        </tr>
                        );
                    })
                    }
                </tbody>
                </table> 
            </div>
        )
    }
}


export class AutorBox extends Component {

    constructor() {
        super();
        this.state = {lista : []};
    }

    componentDidMount(){  
        $.ajax({
            url: api + '/autores',
            dataType: 'json',
            success:(lista) => {
                PubSub.publish('atualiza-lista-autores', lista);
            },
          }
        );

        this.setState(
            { atualizaToken: PubSub.subscribe('atualiza-lista-autores', (topico, lista) => {
                this.setState({lista: lista});
            })
        });
    };

    componentWillUnmount() {
        PubSub.unsubscribe(this.state.atualizaToken);

    }

    render() {
        return (
            <div>
                <div className="header">
                    <h1>Cadastro de Autores</h1>
                </div>
                <div className="content" id="content">

                <FormularioAutor callbackAtualizaListagem={this.atualizaListagem} />
                
                <TabelaAutores lista={this.state.lista}/>            
                </div>
            </div>
        )
    }

}