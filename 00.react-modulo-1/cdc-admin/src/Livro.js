import React, { useState, useEffect } from 'react';
import InputCustomizado from './components/InputCustomizado';
import SubmitCustomizado from './components/SubmitCustomizado';
import $ from 'jquery';
import PubSub from 'pubsub-js';

import TratadorErros from './TratadorErros';

const api = 'http://cdc-react.herokuapp.com/api';

function FormularioLivro(){

    let autores = [];
    let titulo = '';
    let preco = 0;
    let autorId = null;

    setTitulo = (valor) => {
        titulo = valor;
    }

    setPreco = (valor) => {
        preco = valor;
    }

    setAutorId = (valor) => {
        autorId = valor;
    }

    useEffect(() => {
        // Update the document title using the browser API
        const token = PubSub.subscribe('atualiza-lista-autores', (topico, lista) => {
            setAutores(lista);
        });
        $.ajax({
            url: api + '/autores',
            dataType: 'json',
            success:(lista) => {
                PubSub.publish('atualiza-lista-autores', lista);
            },
          }
        );
        // cleanup
        return() => {
            PubSub.unsubscribe(token);
        }
    });
    
    function enviaForm(evento) {
        evento.preventDefault();
        console.log(this);
        console.log("dados sendo enviados");
        $.ajax({
          url: api + '/livros',
          contentType: 'application/json',
          dataType:'json',
          type:'post',
          data: JSON.stringify({
            nome: this.state.titulo,
            email: this.state.preco,
            senha: this.state.autorId,
          }),
          success: (novaListagem) => {
            console.log("enviado com sucesso");
            //disparar um aviso geral de novaListagem disponivel
            PubSub.publish('atualiza-lista-livros', novaListagem);
          },
          error: (resposta) => {
              if(resposta.status === 400) {
                  new  TratadorErros().publicarErros(resposta.responseJSON);
              }
          },
          beforeSend: () => {
              PubSub.publish('limpra-erros-form', '');
          }
        });
      };

    return (
        <div className="pure-form pure-form-aligned">
            {
            /*
            Referência para sintaxe do 'onSubmit' abaixo,
            necessário ser assim para fazer o 'bind' do 'this' no método.
            https://reactjs.org/docs/handling-events.html
            */
            }
            <form className="pure-form pure-form-aligned" onSubmit={(e) => enviaForm(e)} method="post">
            <select value={ autorId } name="autorId" onChange={ (e) => setAutorId(e.target.value) }>
            <option value="">Selecione</option>
            { 
                autores.map(function(autor) {
                return <option key={ autor.id } value={ autor.id }>
                            { autor.nome }
                        </option>;
                })
            }
            </select>
            <InputCustomizado id="titulo" type="text" name="titulo" value={titulo} onChange={(e) => setTitulo(e.target.value)} label="Título"/>                                              
            <InputCustomizado id="preco" type="number" name="preco" value={preco} onChange={(e) => setPreco(e.target.value)} label="Preço"/>                                              
            <SubmitCustomizado caption="Gravar"/>
            </form>             

        </div>
    )
}


function TabelaLivros(props) {
    return (
        <div>            
            <table className="pure-table">
            <thead>
                <tr>
                <th>Título</th>
                <th>Preço</th>
                </tr>
            </thead>
            <tbody>
                {
                props.lista.map((livro) => {
                    return (
                    <tr key={livro.id}>
                        <td>{livro.titulo}</td>
                        <td>{livro.email}</td>
                    </tr>
                    );
                })
                }
            </tbody>
            </table> 
        </div>
    )
}


export function LivroBox(){

    // const [lista, setLista] = useState([]);
    let lista = [];

    useEffect(() => {
        const token = PubSub.subscribe('atualiza-lista-livros', (topico, novaLista) => {
            lista = novaLista;
        });

        $.ajax({
            url: api + '/livros',
            dataType: 'json',
            success:(lista) => {
                PubSub.publish('atualiza-lista-livros', lista);
            },
        });

        return () => {
            PubSub.unsubscribe(token);
        };
    });

    return (
        <div>
            <div className="header">
                <h1>Cadastro de Livros</h1>
            </div>
            <div className="content" id="content">

            <FormularioLivro/>
            
            <TabelaLivros lista={lista}/>            
            </div>
        </div>
    )

}